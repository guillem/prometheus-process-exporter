prometheus-process-exporter (0.7.5-1) UNRELEASED; urgency=medium

  [ Daniel Swarbrick ]
  * New upstream release. Closes: #948002.
  * Drop repackaged branch, since upstream removed vendor directory.
  * Add patches to allow building without vendored deps:
    - 02-vendor-ncabatoff-go-seq-seq.patch
    - 03-disable-fakescraper.patch
  * Add new build-dep golang-github-prometheus-common-dev.
  * Update Standards-Version to 4.5.0 (no changes).
  * Update debhelper-compat to 13.
  * Update lintian-overrides.
  * Add upstream metadata.

  [ Guillem Jover ]
  * Update and unify default file. Closes: #953651.
  * Comment out disabled option in man page.
  * Build-Depend on golang-github-prometheus-procfs-dev (>= 0.2.0) instead of
    golang-procfs-dev.
  * Switch to the golang-github-ncabatoff-go-seq-dev package instead of
    patching the code in.
  * Add a gitlab CI configuration file.

 -- Daniel Swarbrick <daniel.swarbrick@cloud.ionos.com>  Mon, 16 Nov 2020 20:11:19 +0100

prometheus-process-exporter (0.4.0+ds-1) unstable; urgency=medium

  [ Frédéric Bonnard ]
  * Fix TestReadFixture on non 4K page size arches. TestReadFixture checks
    fixtures/14804/stat RSS which is a number of page and compares it to
    hardcoded expected value 0x7b1000 (= 1969 * 4096). Instead of using a
    4k expected value, use system's pagesize like it's done in
    proc_stat.go. Closes: #915035.

  [ Martina Ferrari ]
  * Automated fixes from cme.
  * Prefix patch with series numner.
  * Add debian/gbp.conf to describe repo configuration.
  * Claim /var/lib/prometheus directory. Closes: #915782.
  * Use DH_GOLANG_INSTALL_EXTRA instead of linking test fixtures.
  * Add myself to uploaders.
  * Add '+ds' suffix to version number to reflect repackaging done to remove
    vendoring.
  * Update watchfile for repackaging suffix.

 -- Martina Ferrari <tina@debian.org>  Mon, 14 Jan 2019 01:33:35 +0000

prometheus-process-exporter (0.4.0-1) unstable; urgency=medium

  * Initial release. Closes: #911939.

 -- Daniel Swarbrick <daniel.swarbrick@cloud.ionos.com>  Fri, 26 Oct 2018 17:12:06 +0200
