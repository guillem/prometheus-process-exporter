Source: prometheus-process-exporter
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Daniel Swarbrick <daniel.swarbrick@cloud.ionos.com>,
           Martina Ferrari <tina@debian.org>,
Section: net
Testsuite: autopkgtest-pkg-go
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-golang,
               golang-github-google-go-cmp-dev,
               golang-github-ncabatoff-go-seq-dev,
               golang-github-prometheus-client-golang-dev,
               golang-github-prometheus-common-dev,
               golang-github-prometheus-procfs-dev (>= 0.2.0),
               golang-go,
               golang-gopkg-check.v1-dev,
               golang-yaml.v2-dev,
Standards-Version: 4.5.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/go-team/packages/prometheus-process-exporter
Vcs-Git: https://salsa.debian.org/go-team/packages/prometheus-process-exporter.git
Homepage: https://github.com/ncabatoff/process-exporter
XS-Go-Import-Path: github.com/ncabatoff/process-exporter

Package: prometheus-process-exporter
Architecture: any
Depends: ${misc:Depends},
         ${shlibs:Depends},
Built-Using: ${misc:Built-Using},
Description: Prometheus exporter that exposes process metrics from procfs
 Some apps are impractical to instrument directly, either because you don't
 control the code or they're written in a language that isn't easy to
 instrument with Prometheus. This exporter solves that issue by mining
 process metrics from procfs.
